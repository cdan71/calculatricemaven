/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.inclusiv.cdan7.calculatricemaven;

import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author ornel
 */
public class CalculatriceNGTest {
    
    public CalculatriceNGTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }

    /**
     * Test of addition method, of class Calculatrice.
     */
    @Test
    public void testAddition() {
        System.out.println("addition");
        double nbr1 = 1.0;
        double nbr2 = 1.0;
        Calculatrice instance = new Calculatrice();
        double expResult = 2.0;
        double result = instance.addition(nbr1, nbr2);
        assertEquals(result, expResult, 0.0);
       
    }

    /**
     * Test of soustraction method, of class Calculatrice.
     */
    @Test
    public void testSoustraction() {
        System.out.println("soustraction");
        double nbr1 = 4.0;
        double nbr2 = 2.0;
        Calculatrice instance = new Calculatrice();
        double expResult = 2.0;
        double result = instance.soustraction(nbr1, nbr2);
        assertEquals(result, expResult, 0.0);
        
    }

    /**
     * Test of multiplication method, of class Calculatrice.
     */
    @Test
    public void testMultiplication() {
        System.out.println("multiplication");
        double nbr1 = 2.0;
        double nbr2 = 2.0;
        Calculatrice instance = new Calculatrice();
        double expResult = 4.0;
        double result = instance.multiplication(nbr1, nbr2);
        assertEquals(result, expResult, 0.0);
       
    }

    /**
     * Test of division method, of class Calculatrice.
     */
    @Test
    public void testDivision() {
        System.out.println("division");
        double nbr1 = 4.0;
        double nbr2 = 2.0;
        Calculatrice instance = new Calculatrice();
        double expResult = 2.0;
        double result = instance.division(nbr1, nbr2);
        assertEquals(result, expResult, 0.0);
        
    }

    /**
     * Test of moyenne method, of class Calculatrice.
     */
    @Test
    public void testMoyenne() {
        System.out.println("moyenne");
        int[] n = {10,20};
        Calculatrice instance = new Calculatrice();
        double expResult = 15.0;
        double result = instance.moyenne(n);
        assertEquals(result, expResult, 0.0);
       
    }
    
}
