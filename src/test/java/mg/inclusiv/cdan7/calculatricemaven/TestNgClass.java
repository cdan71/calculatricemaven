/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.inclusiv.cdan7.calculatricemaven;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author ornel
 */
public class TestNgClass {
    
     private WebDriver driver;

    @BeforeClass
    public void setUp() {
        // Set the path to the chromedriver executable
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\ornel\\OneDrive\\Desktop\\qualimetrie\\chromedriver-win64\\chromedriver.exe");

        // Create an instance of ChromeDriver
        driver = new ChromeDriver();
    }

    @Test
    public void testGoogleSearch() {
        // Open Google homepage
        driver.get("https://www.google.com");

        // Find the search input field
        WebElement searchInput = driver.findElement(By.name("q"));

        // Enter a search query
        searchInput.sendKeys("TestNG with Selenium");

        // Submit the search form
        searchInput.submit();

        // Verify the search results
        WebElement searchResults = driver.findElement(By.id("search"));
        // Perform assertions or verifications as needed
    }

    @AfterClass
    public void tearDown() {
        // Quit the browser
        driver.quit();
    }
    
}
