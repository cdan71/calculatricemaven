/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.inclusiv.cdan7.calculatricemaven;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CalculatriceMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        
        //Testing with selenium
        
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\ornel\\OneDrive\\Desktop\\qualimetrie\\chromedriver-win64\\chromedriver.exe");
       
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.kibo.mg/");
       
        driver.findElement(By.xpath("//*[@id=\"search_widget\"]/form/input[2]")).sendKeys("Riz"); 
       
        
        System.out.println("manoratra sucre");
       
        driver.findElement(By.xpath("//*[@id=\"search_widget\"]/form/button/i")).click();
        System.out.println("mande ny click");
        
        
        WebElement products = driver.findElement(By.xpath("//*[@id=\"js-product-list\"]/div[1]"));

        WebElement firstArticle = products.findElement(By.cssSelector(":first-child"));

        firstArticle.click();
        
        System.out.println("mande ny click 1er element"); 
        
        driver.findElement(By.xpath("//*[@id=\"add-to-cart-or-refresh\"]/div[3]/div/div[1]/div/span[3]/button[1]")).click();
        System.out.println("mande ny increment 2");
        
       
        driver.findElement(By.xpath("//*[@id=\"add-to-cart-or-refresh\"]/div[3]/div/div[2]/button")).click();
        System.out.println("mande ny commande");
        
        WebDriverWait wait = new WebDriverWait(driver, 5);
        WebElement lien = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='blockcart-modal']/div/div/div[2]/div/div[2]/div/div/a")));

        lien.click();
        
        String article = driver.findElement(By.xpath("//*[@id=\"main\"]/div[1]/div[1]/div[1]/div[2]/ul/li/div/div[2]/div[1]/a")).getText();
     
        String quantite = driver.findElement(By.xpath("//*[@id=\"cart-subtotal-products\"]/span[1]")).getText();

        
        System.out.println(article +" et "+ quantite);
        
    
      
        
       if (article.toLowerCase().contains("Riz".toLowerCase()) && ( quantite.equalsIgnoreCase("2 aticles"))){
             
            
           // driver.findElement(By.xpath("/html/body/div/div[2]/div[4]/button/div/svg/path")).click();
         
            WebDriverWait poly = new WebDriverWait(driver, 5);
            WebElement rohy = poly.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"main\"]/div[1]/div[2]/div/div[2]/div/a")));
            rohy.click();
            
           
            
            File screenshotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

            // Specify the destination path and file name for the screenshot
            String destinationPath = "C:\\Users\\ornel\\OneDrive\\Desktop\\qualimetrie\\screenshot.png";

            // Save the screenshot to the specified location
            try {
            FileUtils.copyFile(screenshotFile, new File(destinationPath));
            System.out.println("Screenshot saved to: " + destinationPath);
            }catch (IOException e) {
            e.printStackTrace();
            }
        
             
        }else{
            System.out.println("Test failed");
        }
        
       // TEST NG
       
       
    } 
}


    
 